# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## What is in this repository

In this repository is a program meant to be ran through pymongo within docker containers, it is a program where you put in control times as per the project 4 specifications, however data is also stored in a database which will be displayed when you press the "display" button

## Test Cases

When the whole submission field is all blank then the submission button instead does nothing at all, not even creating console.logs.

When nothing is submitted Display should show an empty page as there is no data entered.

## Turn in Instructions

You'll turn in your credentials.ini using which we will get the following:

* The working application.

* A README.md file that includes not only identifying information (your name) but but also a revised, clear specification 
  of the brevet controle time calculation rules.

* Dockerfile

* Test cases for the two buttons. No need to run nose.

* docker-compose.yml

## Contact Info

Maintained by: Edison Mielke 

edisonm@uoregon.edu

enmielke@gmail.com