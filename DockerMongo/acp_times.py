"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow


#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.

def num_checks(int_or_float):
    # should be impossible, but better safe than sorry.
    if (isinstance(int_or_float, int)) or (isinstance(int_or_float, float)):
        return 0
    else:
        return 1


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    speeds = [34, 32, 30, 28, 28, 26]
    DISTANCE_DIV = 200
    total_hours = 0
    total_minutes = 0
    distance_cycles = 0

    # works perfectly for french

    if num_checks(control_dist_km):
        raise Exception("Invalid Km Input!")
    if control_dist_km > DISTANCE_DIV:
        distance_cycles = int(control_dist_km // DISTANCE_DIV) #<---- Something is going out of range on this line over 200
        control_dist_km = (control_dist_km % DISTANCE_DIV)
        for i in range(distance_cycles):
            total_hours += DISTANCE_DIV // speeds[i]
            total_minutes += ((DISTANCE_DIV % speeds[i]) / speeds[i]) * 60
    total_hours += (control_dist_km // speeds[distance_cycles])
    total_minutes += (control_dist_km % speeds[distance_cycles]) / speeds[distance_cycles] * 60
    distance_cycles += 1
    if total_minutes >= 60:
        total_hours += total_minutes // 60
        total_minutes = total_minutes % 60
    new_date = brevet_start_time.shift(hours=total_hours, minutes=round(total_minutes))
    return new_date.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    speeds = [15, 15, 15, 11.428, 11.428, 13.333] # breaks up the speeds into 200km chunks
    DISTANCE_DIV = 200
    total_hours = 0
    total_minutes = 0
    distance_cycles = 0

    # works perfectly for french
    if num_checks(control_dist_km):
        raise Exception("Invalid Km Input!")
    if control_dist_km < 60:
        total_hours += control_dist_km // 20
        total_hours += 1
        total_minutes += ((control_dist_km % 20) / 20) * 60
    else:
        if control_dist_km > DISTANCE_DIV:
            distance_cycles = int(control_dist_km // DISTANCE_DIV) #<---- Something is going out of range on this line over 200
            control_dist_km = (control_dist_km % DISTANCE_DIV)
            for i in range(distance_cycles):
                total_hours += DISTANCE_DIV // speeds[i]
                total_minutes += ((DISTANCE_DIV % speeds[i]) / speeds[i]) * 60
        total_hours += (control_dist_km // speeds[distance_cycles])
        total_minutes += (control_dist_km % speeds[distance_cycles]) / speeds[distance_cycles] * 60
        distance_cycles += 1
    if total_minutes >= 60:
        total_hours += total_minutes // 60
        total_minutes = total_minutes % 60
    new_date = brevet_start_time.shift(hours=total_hours, minutes=round(total_minutes))
    return new_date.isoformat()
