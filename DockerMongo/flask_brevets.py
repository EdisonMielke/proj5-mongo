"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
# client = MongoClient('mongodb://mongodb:27017/')
#client = MongoClient('172.17.0.2', 27017)
#client.server_info() #Used for troubleshooting ip, shouldn't be needed
db = client.appdb


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    intcheck = request.args.get('km', type=str)
    km = request.args.get('km', 1201, type=float)
    distance = request.args.get('total_distance', type=int)  # added a distance variable so that brevets aren't stuck to
    # 200 km
    datetime = request.args.get('datetime', type=str)  # added a datetime variable based on the date and time from
    # the html file
    app.logger.debug("km={}".format(km))

    errors = ""
    if intcheck.isdigit():
        if int(km) >= 1200:
            errors = "This Brevit checkpoint is past the maximum Brevit length"
            result = {"open": "1999-01-01T00:00:00+00:00", "close": "1999-01-01T00:00:00+00:00", "errors": errors}
            return flask.jsonify(result=result)
        elif int(km) > distance:
            errors = "This Brevit checkpoint is past the Brevit length"
    else:
        errors = "Please enter a number in the 'Miles' or 'Km' Box"
        result = {"open": "1999-01-01T00:00:00+00:00", "close": "1999-01-01T00:00:00+00:00", "errors": errors}
        return flask.jsonify(result=result)

    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, arrow.get(datetime))
    close_time = acp_times.close_time(km, distance, arrow.get(datetime))
    result = {"open": open_time, "close": close_time, "errors": errors}
    return flask.jsonify(result=result)


@app.route("/_submitFunction")
def _submitFunction():
    app.logger.debug("Got a JSON request for: SUBMIT: ")
    item_doc = {
        'open_time_field': request.args['open_time_field'],
        'close_time_field': request.args['close_time_field']
    }

    db.appdb.insert_one(item_doc)
    result = "Submission Added"
    return flask.jsonify(result=result)


@app.route("/display", methods=['POST'])
def _displayFunction():
    app.logger.debug("Got a JSON request for: DISPLAY: ")
    _items = db.appdb.find()
    items = [item for item in _items]
    return flask.render_template('/display.html', items=items)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
